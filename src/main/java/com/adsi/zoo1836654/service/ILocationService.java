package com.adsi.zoo1836654.service;

import com.adsi.zoo1836654.domian.Location;
import org.springframework.http.ResponseEntity;

public interface ILocationService {

    public Location create(Location location);

    public Iterable<Location> read();
}
