package com.adsi.zoo1836654.service;

import com.adsi.zoo1836654.domian.Location;
import com.adsi.zoo1836654.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ILocationServiceImp implements ILocationService {

    @Autowired
    private LocationRepository locationRepository;

    @Override
    public Location create(Location location) {
        return locationRepository.save(location);
    }

    @Override
    public Iterable<Location> read() {
        return locationRepository.findAll();
    }
}
