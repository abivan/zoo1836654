package com.adsi.zoo1836654.web.rest;

import com.adsi.zoo1836654.domian.LocationType;
import com.adsi.zoo1836654.service.ILocationTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class LocationTypeResource {

    @Autowired
    ILocationTypeService locationTypeService;

    @PostMapping("/location-type")
    public LocationType create(@RequestBody LocationType locationType){
        return locationTypeService.create(locationType);
    }

    @GetMapping("/location-type")
    public Iterable<LocationType> read(){
        return locationTypeService.read();
    }

}
