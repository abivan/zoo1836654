package com.adsi.zoo1836654.web.rest;

import com.adsi.zoo1836654.domian.Animal;
import com.adsi.zoo1836654.repository.dto.AnimalNameGender;
import com.adsi.zoo1836654.service.IAnimalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/animal")
public class AnimalResource {

    @Autowired
    IAnimalService animalService;

    @PostMapping("")
    public ResponseEntity create(@RequestBody Animal animal){
        return animalService.create(animal);
    }

    @GetMapping("")
    public Iterable<Animal> read(){
        return animalService.read();
    }

    @GetMapping("/count")
    public Integer countAnimals(){
        return animalService.quantityAnimals();
    }

    @GetMapping("/name-gender")
    public Iterable<AnimalNameGender> getAnimalNameGender(){
        return animalService.getAnimalNameGender();
    }
}
