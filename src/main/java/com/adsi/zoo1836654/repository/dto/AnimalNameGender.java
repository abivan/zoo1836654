package com.adsi.zoo1836654.repository.dto;

public interface AnimalNameGender {

    public String getName();
    public String getGender();
}
