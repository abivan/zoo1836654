package com.adsi.zoo1836654.repository;

import com.adsi.zoo1836654.domian.Location;
import org.springframework.data.repository.CrudRepository;

public interface LocationRepository extends CrudRepository<Location, Integer> {
}
