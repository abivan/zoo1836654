package com.adsi.zoo1836654.repository;

import com.adsi.zoo1836654.domian.LocationType;
import org.springframework.data.repository.CrudRepository;

public interface LocationTypeRepository extends CrudRepository<LocationType, Integer> {
}
